To release a new `cas` version, follow the steps below:

1. If the CLI interface or the help messages have been modified:
   * Run `make docs/cmd` to regenerate the Markdown CLI documentation.
2. Modify the `VERSION` variable on the `Makefile`, omitting the `v` prefix: `VERSION=X.Y.Z`
3. Run `make CHANGELOG.md.next-tag`.
4. Commit changes.
5. Open a pull request to push all the code to the `master` branch.
6. Once the code is merged, open the Jenkins pipeline.
7. Click on the `Build with Parameters` button.
8. If the release can be created from the latest commit:
   * Enter the version on the pipeline field in the format
   `vX.Y.Z`, following the [Semantic Versioning](https://semver.org/spec/v2.0.0.html) convention. The job will create the tag automatically.
9. If it cannot:
   * Create a tag for the version on the repository with a command like `git tag -a vX.Y.Z`.
10. Add the release notes on the `release_notes` field.
11. Click the `Build` button.
12. The first step will wait for your input regarding the tag creation. Select the option appropriately, depending on what you did for steps 8 and 9.
13. The pipeline will build the binaries, notarize them and upload them to GitHub,
together with the release notes.
14. Update the [cas Homebrew formula](https://github.com/vchain-us/homebrew-brew/blob/master/Formula/cas.rb).
