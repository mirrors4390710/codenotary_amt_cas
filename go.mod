module github.com/codenotary/cas

go 1.13

require (
	github.com/CycloneDX/cyclonedx-go v0.4.0
	github.com/Microsoft/go-winio v0.4.17 // indirect
	github.com/anchore/go-rpmdb v0.0.0-20210602151223-1f0f707a2894
	github.com/blang/semver v3.5.1+incompatible
	github.com/caarlos0/spin v1.1.0
	github.com/codenotary/immudb v1.2.4
	github.com/dghubble/sling v1.3.0
	github.com/docker/distribution v2.8.1+incompatible // indirect
	github.com/docker/docker v20.10.18+incompatible
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.5.0 // indirect
	github.com/dustin/go-humanize v1.0.0
	github.com/fatih/color v1.13.0
	github.com/gofrs/flock v0.8.1
	github.com/google/go-cmp v0.5.9
	github.com/google/uuid v1.3.0
	github.com/h2non/filetype v1.0.10
	github.com/kr/pretty v0.2.1 // indirect
	github.com/mattn/go-colorable v0.1.12
	github.com/matttproud/golang_protobuf_extensions v1.0.2-0.20181231171920-c182affec369 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/moby/term v0.0.0-20200312100748-672ec06f55cd // indirect
	github.com/morikuni/aec v1.0.0 // indirect
	github.com/opencontainers/go-digest v1.0.0
	github.com/opencontainers/image-spec v1.0.2 // indirect
	github.com/package-url/packageurl-go v0.1.0
	github.com/schollz/progressbar/v3 v3.7.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.3.0
	github.com/spf13/viper v1.10.1
	github.com/stretchr/testify v1.7.0
	github.com/vchain-us/ledger-compliance-go v0.9.3-0.20220118134549-9591b15eb645
	golang.org/x/crypto v0.0.0-20220525230936-793ad666bf5e
	golang.org/x/net v0.0.0-20220524220425-1d687d428aca // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a // indirect
	google.golang.org/genproto v0.0.0-20220525015930-6ca3db687a9d // indirect
	google.golang.org/grpc v1.46.2
	gopkg.in/src-d/go-git.v4 v4.13.1
	gopkg.in/yaml.v3 v3.0.0 // indirect
)

replace github.com/spf13/afero => github.com/spf13/afero v1.5.1
